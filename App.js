import React, { Component } from 'react';
import SignUp from './App/Screens/SignUpScreen/SignUp';
import store  from './App/Redux/Store';
import { Provider } from 'react-redux';
import AuthStack from './App/Navigation/Router';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AuthStack />
      </Provider>
    )
  }
}


export default App;