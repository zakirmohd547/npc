import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../Screens/LoginScreen/Login';
import DashboardScreen from '../Screens/DashboardScreen/Dashboard';
import SignUpScreen from '../Screens/SignUpScreen/SignUp';
import Calendar from '../Screens/Calendar/Calendar';
import Account from '../Screens/Account/Account';
import Splash from '../Screens/Splash/Splash';
import Welcome from '../Screens/Welcome/Welcome';
import ForgotPasswordScreen from '../Screens/ForgotPasswordScreen/ForgotPassword';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/MaterialCommunityIcons';


const Stack = createStackNavigator();
const ShopStack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HomeStack = (props) => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}>
            <Stack.Screen name="Dashboard" component={DashboardScreen} />
        </Stack.Navigator>
    );
};
const SplashStack = (props) => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}>
        <Stack.Screen name="Splash" component={Splash} />        
        <Stack.Screen name="Welcome" component={Welcome} />        
    </Stack.Navigator>
    );
};



const BottomTab = props => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                    iconName = focused
                        ? 'home-account'
                        : 'home-account';
                } else if (route.name === 'Account') {
                    iconName = focused ? 'account' : 'account';
                }else if (route.name === 'Calendar') {
                    iconName = focused ? 'calendar' : 'calendar';
                }

                // You can return any component that you like here!
                return <Ionicons name={iconName} size={size} color={color} />;
            },
        })}
        tabBarOptions={{
            activeTintColor: '#191970',
            inactiveTintColor: 'gray',
        }}
    >
        <Tab.Screen name="Home" component={HomeStack}
        />
        <Tab.Screen name="Account" component={Account} />
        <Tab.Screen name="Calendar" component={Calendar} />
    </Tab.Navigator>
);

function AuthStack() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false,
                }}
                initialRouteName={'Splash'}>
                <Stack.Screen name="Splash" component={SplashStack} />
                <Stack.Screen name="Login" component={LoginScreen} />
                <Stack.Screen name="SignUp" component={SignUpScreen} />
                <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
                <Stack.Screen name="Dashboard" component={BottomTab} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default AuthStack;
