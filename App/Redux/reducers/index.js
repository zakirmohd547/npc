import {
    SIGNUP_REQUEST,
    SIGNUP_SUCCESS,
    SIGNUP_FAILURE,

    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,

} from "../constants/actionTypes";



export const initialState = {
    isLoading:false,
    data:null,
    errorMessage:''
}

// login
export const loginReducer=(state=initialState,action)=>{
    switch(action.type){
        case LOGIN_REQUEST:
            return {...state,isLoading:true}
        case LOGIN_SUCCESS:
            return {...state,isLoading:false,data: action.payload}
        case LOGIN_FAILURE:
            return {...state,isLoading:false,errorMessage:action.payload}
        default:
            return state;
    }
}
// Register
export const registerReducer=(state=initialState,action)=>{
    switch(action.type){
        case SIGNUP_REQUEST:
            return {...state,isLoading:true}
        case SIGNUP_SUCCESS:
            return {...state,isLoading:false,data: action.payload}
        case SIGNUP_FAILURE:
            return {...state,isLoading:false,errorMessage:action.payload}
        default:
            return state;
    }
}
