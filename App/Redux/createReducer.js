import { combineReducers } from "redux";
import {loginReducer,registerReducer} from "./reducers/index";

export default combineReducers({
loginData:loginReducer,
registerData:registerReducer
});