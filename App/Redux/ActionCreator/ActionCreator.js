import * as Action from '../actions/index';
import * as Constant from '../../Utility/constant';
import {callApi} from '../../Utility/BaseModel';
// login 
export const loginUserAsync = (requestBody) => {
    return async dispatch => {
      dispatch(Action.loginRequest(requestBody));
      try {
        let data= callApi(Constant.METHOD_TYPE_POST,Constant.API_LOGIN,requestBody)
        let responseData=await data;
        console.log("responseData",responseData)
        
        dispatch(Action.loginSuccess(responseData))
      } catch (error){ 
        dispatch(Action.loginFailure(error));
      }
    };
  };

// register
export const RegisterUserAsync = (requestBody) => {
    return async dispatch => {
      dispatch(Action.registerRequest(requestBody));
      try {
        let data= callApi(Constant.METHOD_TYPE_POST,Constant.API_REGISTER,requestBody)
        let responseData=await data;
        console.log("responseData",responseData)
        dispatch(Action.registerSuccess(responseData))
      } catch (error){ 
        dispatch(Action.registerFailure(error));
      }
    };
  };