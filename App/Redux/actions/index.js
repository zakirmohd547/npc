import {
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from '../constants/actionTypes';

// Login action method
export const loginRequest = requestBody => ({
  type: LOGIN_REQUEST,
  payload: requestBody,
});
export const loginSuccess = response => ({
  type: LOGIN_SUCCESS,
  payload: response,
});
export const loginFailure = err => ({
  type: LOGIN_FAILURE,
  payload: err,
});
// Registeer action method
export const registerRequest = requestBody => ({
  type: SIGNUP_REQUEST,
  payload: requestBody,
});
export const registerSuccess = response => ({
  type: SIGNUP_SUCCESS,
  payload: response,
});
export const registerFailure = err => ({
  type: SIGNUP_FAILURE,
  payload: err,
});


// export const loginUserAsync = (requestBody) => {
//   return async dispatch => {
//     dispatch(loginRequest(requestBody));
//     try {
//       let data= await fetch('http://abhgroup.in/admin.abhgroup.in/mobileapi/user_login.php', {
//         method: 'POST',
//         headers: {
//           Accept: 'application/json',
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify(requestBody),
//       });
//       let responseData= await data.json();
//      console.log("responseData",responseData)
//       dispatch(loginSuccess(data))
//     } catch (error){ 
//       dispatch(loginFailure(error));
//     }
//   };
// };
