import reducer from './createReducer';
import { logger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'

const store = createStore(reducer, applyMiddleware(thunk, logger))

export default store;
