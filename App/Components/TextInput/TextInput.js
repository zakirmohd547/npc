import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './TextInputStyle';

class TextInputComponent extends Component {
    render() {
        const { iconName, placeholder, editable, updateFields, backgroundColor, multiline, camera } = this.props;
        if (editable) {
            return (
                <View style={[styles.mainContainer]}>
                    <View style={[styles.iconContainer]}>
                        <Icon name={iconName} size={25} color="#fff" />
                    </View>
                    <TextInput style={[styles.textInput, { fontSize: 15 }]}
                        placeholder={placeholder}
                        placeholderTextColor='#fff'
                        editable={editable}
                        isFocused={true}
                        onChangeText={text => updateFields(text)}
                        keyboardType={placeholder == "Phone" ? "number-pad" : "default"}
                        secureTextEntry={placeholder == "Password" || placeholder == "Confirm Password" ? true : false} />
                </View>
            );
        }
        else {
            return (
                <TouchableOpacity style={[styles.mainContainer, styles.image]}
                    onPress={() => updateFields()}>
                    <View style={[styles.iconContainer, { backgroundColor: '#ccc' }]}>
                        <Icon name={iconName} size={20} color="#000" />
                    </View>
                    <TextInput style={[styles.textInput, { fontSize: 20 }]}
                        placeholder={placeholder}
                        placeholderTextColor='#000'
                        editable={editable}
                    />
                </TouchableOpacity>
            );
        }

    }
}



export default TextInputComponent;