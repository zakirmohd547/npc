import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    mainContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        borderRadius: 35,
        height: 50,
        marginBottom: 10,
        width: '85%',
        paddingHorizontal: 6,
        margin: 10,
        borderColor:'#ccc',
        borderWidth:2
       // shadowColor:'#000',
      //  elevation:5
    },
    image: {
        width: "95%",
        backgroundColor: '#ccc',
        borderRadius: 20
    },
    iconContainer: {
        height: 45,
        width: '14%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 2,
        marginLeft:5,
    },
    textInput: {
        paddingHorizontal: 10,
        width: '90%',
        paddingVertical: 0,
        color: '#fff',
    },
    image: {
        width: "85%",
        backgroundColor: '#ccc',
        borderRadius: 20
    },
    multilines: {
        width: '85%',
        backgroundColor: '#ccc',
        height: 100,
        alignItems: 'flex-start'
    },
    multilineInput: {
        color: '#000',
        marginTop: 8,
        fontSize: 16
    },
});

export default styles;


