import React from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import styles from './CustomHeaderStyle';
import Ionicons from 'react-native-vector-icons/FontAwesome';
import {Badge, Avatar} from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const HomeHeader = props => {
  return (
    <View style={styles.mainComponent}>
      <TouchableOpacity
        style={styles.drawerIcon}
        onPress={() => console.log('drawer')}>
  
        <Ionicons name={'list-ul'} size={30} color={'#fff'} />
       
      </TouchableOpacity>
      <View style={styles.headingContainer}>
        <Text style={styles.headingText}>{props.title}</Text>
      </View>
      <TouchableOpacity
        style={styles.cart}
        onPress={() => console.log('drawer')}>
        <Avatar
        rounded 
        size={50}
  source={require('../../Assets/Images/user.jpg')}
  />
        {/* <Badge
          value={props.value}
          status="success"
          textStyle={{color:'#ff5257'}}
          badgeStyle={{backgroundColor:'#fff',width:19,height:19}}
          containerStyle={{position: 'absolute',left:wp('60%'),top:-6,}}
        /> */}
      </TouchableOpacity>
    </View>
  );
};

export default HomeHeader;
