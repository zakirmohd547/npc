import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    mainComponent: {
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#191970',
        width: wp('100%'),
        height: hp('8%')
    }, drawerIcon: {
        alignItems: 'flex-start',
        marginLeft: wp('4%'),
        marginTop: hp('2%'),
        width: wp('12%'),

    }, headingContainer: {
        alignItems: 'flex-start',
        marginLeft: 5,
        marginTop: hp('2.5%'),
    }, headingText: {
        fontSize: 18,
        textAlign: 'center',
        color: '#fff',
        fontWeight: 'bold'
    }, cart: {
        alignItems: 'flex-end',
        marginLeft: wp('4%'),
        width: wp('62%'),
        marginTop:wp('1')
    }
})
export default styles;