import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './ProductItemStyle';
class ProductItem extends Component {
    render() {
        const { item, navigation, width } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity style={[styles.mainView, { width: width }]} onPress={() => console.log("")}>
                    <View style={styles.imageView}>
                        <Image source={item.image} style={styles.image} />
                    </View>
                    <View style={styles.detailView}>
                        <Text numberOfLines={1} style={[styles.qautity, { marginLeft: 15 }]}>{item.title}
                        </Text>
                        <Text numberOfLines={2} style={[styles.qautity, { marginLeft: 15 }]}>{item.name}
                        </Text>
                        <Text numberOfLines={1} style={[styles.qautity, { marginLeft: 15 }]}>$ {item.price}
                        </Text>
                        <Text numberOfLines={1} style={[styles.bonus, { marginLeft: 15 }]}>Earn Bonus Point Now
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
export default ProductItem;


