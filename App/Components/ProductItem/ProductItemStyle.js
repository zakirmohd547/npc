import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    mainView: {
        marginTop: 15,
        marginLeft: 10,
        marginRight: 5,
        maxHeight: 'auto',
        borderRadius: 5,
        backgroundColor: '#fff',
        marginBottom:20,
    },
    offView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 40
    },
    off: {
        width: 70,
        marginBottom: 10,
        height: 30,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    offText: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff'
    },
    wishlist: {
        marginRight: 5,
        marginTop: 2
    },
    imageView: {
        width: '100%',
        height: 200,
        alignItems: 'center'
    },
    image: {
        width: '95%',
        height: '95%'
    },
    detailView: {
        width: '100%',
        height: 95
    },
    priceView: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    price: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'red',
        marginLeft: 10,
        marginTop: 5
    },
    qautity: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#000',
        marginRight: 10,
        marginTop: 5,
        textAlign: 'center',
    },
    bonus: {
        fontSize: 15,
        color: '#000',
        marginRight: 10,
        marginTop: 5,
        textAlign: 'center',
    },
    addView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 5
    },
    countView: {
        marginTop: 5,
        width: 25,
        height: '55%',
        borderWidth: 1,
        borderColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center'
    },
    countText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#000'
    },
    bottomView: {
        width: '100%',
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5,
    },
    addtoCartView: {
        width: '80%',
        height: '90%',
        backgroundColor: 'green',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        flexDirection: 'row'
    },
    addText: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff'
    }
});

export default styles;