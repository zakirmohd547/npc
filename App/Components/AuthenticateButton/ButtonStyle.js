import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    button:{
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    mainContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 35,
        height: 50,
        marginBottom: 5,
        width: '85%',
        paddingHorizontal: 6,
        backgroundColor: '#ff5257',
        margin: 10,
        shadowColor:'#000',
        elevation:5
    },
    textStyle: {
        fontSize: 18,
        fontWeight:'bold',
        color:'#fff',
    }
});

export default styles;


