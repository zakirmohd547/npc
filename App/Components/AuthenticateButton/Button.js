import React, { Component } from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import styles from './ButtonStyle';

class ButtonComponent extends Component {
    render() {
        const { title, onPress,navigation } = this.props;
        return (
            <TouchableOpacity onPress={() => onPress()} style={styles.button}>
                <View style={styles.mainContainer}>
                    <Text style={styles.textStyle}>{title}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export default ButtonComponent;