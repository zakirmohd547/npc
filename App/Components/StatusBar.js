import React, { Component } from 'react';
import { StatusBar} from 'react-native';

export default class StatusBars extends Component {
    render() {
        return (
                <StatusBar
                    barStyle="dark-content"
                    hidden={false}
                    backgroundColor="#ffd31d"
                    translucent={false}
                    networkActivityIndicatorVisible={true}
                />
        )
    }
}
