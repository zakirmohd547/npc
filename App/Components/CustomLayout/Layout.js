/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {StyleSheet, SafeAreaView, ImageBackground} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

class Layout extends Component {
  render() {
    return (
      <ImageBackground source={require('../../Assets/Images/bg.jpeg')} style={Styles.container}>
        <SafeAreaView style={{flex: 1}}>
          <KeyboardAwareScrollView>
            {this.props.children}
          </KeyboardAwareScrollView>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Layout;
