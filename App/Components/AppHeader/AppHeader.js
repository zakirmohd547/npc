import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icons from 'react-native-vector-icons/MaterialIcons';
import styles from './AppHeaderStyle';


class AppHeader extends Component {
    render() {
        const { title, navigation, align, onShare } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 40 + 20 }}>
                    <View style={[styles.gradient, { paddingTop: 5, backgroundColor: '#ff5257' }]}>
                        <View style={styles.headerView}>
                            <View style={{ width: '8%', alignItems: 'flex-start' }}>
                                {title === "Shop" ?
                                    null
                                    :
                                    <TouchableOpacity style={styles.iconView} onPress={() => { navigation.goBack(null) }}>
                                        <Icons name="arrow-back" size={25} color="#fff" />
                                    </TouchableOpacity>
                                }
                            </View>
                            <View style={{ width: '84%', alignItems: 'center' }}>
                                <Text style={[styles.textView, { fontSize: 25, fontWeight: 'bold' }]}>{title}</Text>
                            </View>
                            <View style={{ alignItems: 'flex-end', width: '8%' }}>
                                <TouchableOpacity style={styles.iconView} onPress={() => { navigation.navigate('Cart') }}>
                                    <Icons name="search" size={30} color="#fff" />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

export default AppHeader;









