import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    gradient: {
        height: '100%',
        width: '100%',
        paddingHorizontal: 12,
        justifyContent: 'center'
    },
    headerView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 2,
        width: '100%',
    },
    iconView: {
        width: 40,
        height: 40,
        justifyContent: 'center',
    },
    textView: {
        fontSize: 20,
        lineHeight: 28,
        color: '#fff',
    }
});

export default styles;