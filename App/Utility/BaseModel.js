import * as asyncStorage from '../Utility/asyncStorage';
import * as Constants from '../Utility/constant';


export async function callApi(methodType, apiUrl, requestBody) {
    console.log("methodType, apiUrl, requestBody==",methodType, apiUrl, requestBody)
    const userData = await asyncStorage.getItem(asyncStorage.KEY_USERDATA);
    const data=JSON.parse(userData);
    console.log("data",data)

    let requestHeaders = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };

    if (methodType === Constants.METHOD_TYPE_GET) {
        console.log('requestHeaders',requestHeaders);
        let data= await fetch(apiUrl, {
            method: methodType,
            headers:requestHeaders
        })
        let responseData= data.json()
        return responseData;
    }else{

        let data= await fetch(apiUrl, {
            method: methodType,
            headers: requestHeaders,
            body: JSON.stringify(requestBody),
        })

        let responseData= data.json()
        return responseData;
    } 

}

