import Toast from 'react-native-simple-toast';
export const showToast = (message) => {
    setTimeout(() => { Toast.show(message, Toast.CENTER) }, 200)
  }
