import AsyncStorage from '@react-native-async-storage/async-storage';


export async function callApi(methodType, apiUrl, requestBody) {
    let apiResponse
    const reqOpts = {
        method: methodType,
        headers: {
            'Content-Type': 'application/json',
        },
    }
    try {
        const token = await AsyncStorage.getItem('TOKEN')
        if (token) {
            reqOpts.headers.Authorization = `Bearer ${token}`
        }
        if (methodType === 'POST') {
            reqOpts.body = JSON.stringify(requestBody)
        }
        console.log('api',apiUrl+JSON.stringify(requestBody));
        const response = await fetch(apiUrl, reqOpts)
        const data = await response.json()

        apiResponse = {
            data,
            statusCode: response.status,
        }
    } catch (error) {
        console.log('callApi', error)
    }
    return apiResponse
}