

// Methods Type
export const METHOD_TYPE_GET="GET";
export const METHOD_TYPE_POST="POST";
export const METHOD_TYPE_PUT="PUT";
export const METHOD_TYPE_DELETE="DELETE";

/////////////////////////////////////// API NAMES ////////////////////////////////////
export const BASE_URL =
  'http://grahdecor.com/NCUADM/api/';
export const API_LOGIN = BASE_URL + 'login/';
export const API_REGISTER = BASE_URL + 'register_store/';


