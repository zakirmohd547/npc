import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'center'
    },
    flatlist: {
        marginHorizontal: 15,
    },
    categoryView: {
        flexDirection: 'row',
        height: 50,
        backgroundColor: '#fff',
    },
    titleView: {
        width: '95%',
        justifyContent: 'center',
    },
    titleText: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    imageView: {
        width: '5%',
        justifyContent: 'center',
    },
    image: {
        height: 50,
        width: 50,
        borderRadius: 25,
    },
    border: {
        borderColor: '#ccc',
        borderWidth: 0.75,
    }
});

export default styles;