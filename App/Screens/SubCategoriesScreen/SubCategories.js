import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, FlatList } from 'react-native';
import styles from './SubCategoriesStyle';
import ShopHeader from '../../Components/AppHeader/AppHeader';
import Icons from 'react-native-vector-icons/MaterialIcons';
export default class Categories extends Component {
    state = {
        subcategories: [
            {
                id: 1,
                name: 'All Men\'s\ Clothing',
            },
            {
                id: 2,
                name: 'Active wear',
            },
            {
                id: 3,
                name: 'Big & Tall',
            },
            {
                id: 4,
                name: 'Blazers & Sport Coats',
            },
            {
                id: 5,
                name: 'Business Casual',
            },
            {
                id: 6,
                name: 'Coats & Jackets',
            },
            {
                id: 7,
                name: 'Dress Shirts',
            },
            {
                id: 8,
                name: 'Hoodies & Sweatshirts',
            },
            {
                id: 9,
                name: 'Jeans',
            },
            {
                id: 10,
                name: 'Pajamas & Robes',
            },
            {
                id: 11,
                name: 'Pants',
            },
            {
                id: 12,
                name: 'Polos',
            },
            {
                id: 13,
                name: 'Shirts',
            },
            {
                id: 14,
                name: 'Shorts',
            },
        ],
        title: '',
    }

    componentDidMount() {
        const title = this.props.route.params.title;
        this.setState({ title: title });
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ height: 60 }}>
                    <ShopHeader title={this.state.title} navigation={this.props.navigation} />
                </View>
                <View style={styles.MainContainer}>
                    <FlatList
                        contentContainerStyle={styles.flatlist}
                        data={this.state.subcategories}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item, index }) =>
                            <View>
                                <TouchableOpacity style={styles.categoryView} onPress={()=>this.props.navigation.navigate('ProductListing',{title:item.name})}>
                                    <View style={styles.titleView}>
                                        <Text style={styles.titleText}>{item.name}</Text>
                                    </View>
                                    <View style={styles.imageView}>
                                        <Icons name="chevron-right" size={25} color="#000" />
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.border} />
                            </View>
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                    />
                </View>
            </SafeAreaView>
        );
    }
}
