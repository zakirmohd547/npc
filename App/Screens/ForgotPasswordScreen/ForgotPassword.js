import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity,TextInput } from 'react-native';
import TextInputComponent from '../../Components/TextInput/TextInput';
import ButtonComponent from '../../Components/AuthenticateButton/Button';
import styles from './ForgotPasswordStyle';
import LinearGradient from 'react-native-linear-gradient';

export default class Login extends Component {


  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require('../../Assets/Images/bg.jpeg')}
          style={{ flex: 1 }}>
          <View style={styles.container}>
            <View style={{flex:2}}/>
            <View style={styles.textView}>
              {/* <TextInputComponent iconName="email" placeholder="Email Address" editable={true} updateFields={() => { }} /> */}
              <View style={[styles.action, {marginTop: 30}]}>
            <TextInput
              placeholder={'Password'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="default"
              ref={input => {
                this.password = input;
              }}
              blurOnSubmit={false}
              // value={this.state.password}
              onChangeText={(value) => console.log()}
              // secureTextEntry={this.state.secureTextEntry}
              // onSubmitEditing={this.handleLoginSubmit}
            />
          </View>
              <TouchableOpacity
              style={styles.signIn}
              onPress={this.handleLoginSubmit}>
              <LinearGradient
                colors={['#08d4c4', '#01ab9d']}
                style={styles.signIn}>
                <Text
                  style={[
                    styles.textSign,
                    {
                      color: '#FFFFFF',
                    },
                  ]}>
                  {'Forget'}
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

