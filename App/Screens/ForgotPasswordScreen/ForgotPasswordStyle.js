import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  topView: {
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  iconStyle: {
    height: 100,
    width: 100,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  textView: {
    alignItems: 'center',
    flex:4,
  },signIn: {
    width: '90%',
    height: 45,
    marginTop:20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#FFFFFF',
    fontWeight: 'bold',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  colors: {
    color: '#000000',
  },
  forgetText: {
    color: '#ffffff',
    marginTop: 15,
    fontWeight: 'bold',
  },
});

export default styles;