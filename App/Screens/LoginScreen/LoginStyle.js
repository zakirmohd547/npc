import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  topView: {
    alignItems: 'center',
    justifyContent: 'center',

  },
  iconStyle: {
    height: 100,
    width: 100,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  textView: {
    alignItems: 'center',
    flex: 4,
  },
  newUserText: {
    marginTop: 10,
    fontSize: 18,
    fontWeight: 'bold',
    color:'#fff'
  },
});

export default styles;