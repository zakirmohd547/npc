/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable eqeqeq */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Keyboard,
  Platform,
} from 'react-native';
import {Avatar} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Layout from '../../Components/CustomLayout/Layout';
import {connect} from 'react-redux';
import {loginUserAsync} from '../../Redux/ActionCreator/ActionCreator';
import {showToast} from '../../Utility/toast';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import * as asyncStorage from '../../Utility/asyncStorage'
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      deviceId: '',
      fcmToken: '',
      deviceType: '',
      secureTextEntry: true,
    };
  }

  updateSecureTextEntry = () => {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
    });
  };

  //------- Validate login form and Login API--------//
  handleLoginSubmit = () => {
    Keyboard.dismiss();
    if (this.state.email == '') {
      alert('Please enter email');
    } else if (this.state.password == '') {
      alert('Please enter password');
    } else {
      this.props.loginUserAsync({
        email: this.state.email,
        password: this.state.password,
      });
    }
  };

  async componentDidUpdate(prevProps) {
    console.log("this.props.userRegisterData",this.props.loginData)
    console.log("this.props.prevProps.registerData",prevProps.userLoginData.loginData)

    if (this.props.userLoginData.loginData && prevProps.userLoginData.loginData !== this.props.userLoginData.loginData) {
        console.log("this.props.userLoginData.registerData",this.props.userLoginData.loginData)
        if(this.props.userLoginData.loginData.data){
            showToast(this.props.userLoginData.loginData.data.message.message);
            asyncStorage.setItem(asyncStorage.KEY_USERDATA,JSON.stringify(this.props.userLoginData.loginData.data.data))
            if(this.props.userLoginData.loginData.data.status.status=="1" && this.props.userLoginData.loginData.data.status.statuscode=="200"){
              this.props.navigation.navigate('Dashboard');

            }
        }
        
    }
  }


  render() {
    const {navigation} = this.props;

    return (
      <Layout>
        <View style={styles.header}>
          <Avatar size={120} source={require('../../Assets/Images/logo.jpeg')} />
          <Text style={styles.text_header}>{'Login'}</Text>
        </View>
        <View style={[styles.footer]}>
          <View style={styles.action}>
            <TextInput
              placeholder={'Email'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="email-address"
              ref={input => {
                this.email = input;
              }}
              onSubmitEditing={() => {
                this.password.focus();
              }}
              blurOnSubmit={false}
              value={this.state.email}
              onChangeText={value => this.setState({email: value})}
            />
          </View>
          <View style={[styles.action, {marginTop: 30}]}>
            <TextInput
              placeholder={'Password'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="default"
              ref={input => {
                this.password = input;
              }}
              blurOnSubmit={false}
              value={this.state.password}
              onChangeText={value => this.setState({password: value})}
              secureTextEntry={this.state.secureTextEntry}
              onSubmitEditing={this.handleLoginSubmit}
            />
          </View>

          <TouchableOpacity
            onPress={() => console.log("")}>
            <Text style={styles.forgetText}>{'Forgot password?'}</Text>
          </TouchableOpacity>

          <View style={styles.button}>
            <TouchableOpacity
              style={styles.signIn}
              onPress={this.handleLoginSubmit}>
              <LinearGradient
                colors={['#08d4c4', '#01ab9d']}
                style={styles.signIn}>
                <Text
                  style={[
                    styles.textSign,
                    {
                      color: '#FFFFFF',
                    },
                  ]}>
                  {'Login'}
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            <Text style={styles.forgetText}>{"Don't have an account?"}</Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('SignUp')}
              style={[
                styles.signIn,
                {
                  borderColor: '#ccc',
                  backgroundColor: '#3195ea',
                  borderWidth: 1,
                  marginTop: 5,
                },
              ]}>
              <Text
                style={[
                  styles.textSign,
                  {
                    color: '#ffffff',
                  },
                ]}>
                {'Register'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <OrientationLoadingOverlay
                        visible={this.props.userLoginData.loginData.isLoading}
                        color="white"
                        indicatorSize="large"
                        messageFontSize={24}
                        message="Loading Please Wait..."
                    />
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    userLoginData: state,
  };
};

const mapDispatchToProps = {
  loginUserAsync,
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);

const {height} = Dimensions.get('screen');
const height_logo = height * 0.15;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 50,
  },
  footer: {
    paddingHorizontal: 30,
    paddingVertical: 30,
  },
  text_header: {
    justifyContent: 'center',
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 15,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#FFFFFF',
    fontWeight: 'bold',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  colors: {
    color: '#000000',
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  forgetText: {
    color: '#ffffff',
    marginTop: 15,
    fontWeight: 'bold',
  },
});
