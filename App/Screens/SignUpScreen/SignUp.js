/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable eqeqeq */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Keyboard,
  Platform,
} from 'react-native';
import {Avatar} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Layout from '../../Components/CustomLayout/Layout';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import {connect} from 'react-redux';
import {RegisterUserAsync} from '../../Redux/ActionCreator/ActionCreator';
import {showToast} from '../../Utility/toast';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
        name:"",
        email:"",
        password:"",
        phone:"",
        company:"",
        job_title:"",
        job_profile:"",
      deviceId: '',
      fcmToken: '',
      deviceType: '',
      secureTextEntry: true,
    };
  }

  updateSecureTextEntry = () => {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
    });
  };

  componentDidUpdate(prevProps) {
    console.log("this.props.userRegisterData",this.props.userRegisterData)
    console.log("this.props.prevProps.registerData",prevProps.userRegisterData.registerData)

    if (this.props.userRegisterData.registerData && prevProps.userRegisterData.registerData !== this.props.userRegisterData.registerData) {
        console.log("this.props.userRegisterData.registerData",this.props.userRegisterData.registerData)
        if(this.props.userRegisterData.registerData.data){
            showToast(this.props.userRegisterData.registerData.data.message.message);
            if(this.props.userRegisterData.registerData.data.status.status=="1" && this.props.userRegisterData.registerData.data.status.statuscode=="200")
            this.props.navigation.navigate('Login');
        }
        
    }
  }
  //------- Validate login form and Login API--------//
  handleLoginSubmit = () => {
    Keyboard.dismiss();
    const {name,email,password,phone,company,job_title,job_profile}=this.state
        if(!name){
            alert("Please enter full name");
        }else if(!email){
            alert("Please enter email");
        }else if(!password){
            alert("Please enter password");
        }else if(!phone){
            alert("Please enter phone");
        }else if(!company){
            alert("Please enter company");
        }else if(!job_title){
            alert("Please enter Job Title");
        }else if(!job_profile){
            alert("Please enter Job Profile");
        }else{
           console.log("name,email,password,phone,company,job_title,job_profile",this.state.name,email,password,phone,company,job_title,job_profile) 
           this.props.RegisterUserAsync({
            name:name,
            email:email,
            password:password,
            phone:phone,
            company:company,
            job_title:job_title,
            job_profile:job_profile}
           )
        }
  };

  render() {
    const {navigation} = this.props;

    return (
      <Layout>
        <View style={styles.header}>
          <Avatar size={120} source={require('../../Assets/Images/logo.jpeg')} />
          <Text style={styles.text_header}>{'Registration'}</Text>
        </View>
        <View style={[styles.footer]}>
          <View style={styles.action}>
            <TextInput
              placeholder={'Full name'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="email-address"
             
              blurOnSubmit={false}
              value={this.state.name}
              onChangeText={name => this.setState({name: name})}
            />
          </View>
          <View style={[styles.action, {marginTop: 30}]}>
            <TextInput
              placeholder={'Email Address'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="email-address"
            
              value={this.state.email}
              onChangeText={value => this.setState({email: value})}
            />
          </View>
          <View style={[styles.action, {marginTop: 30}]}>
            <TextInput
              placeholder={'Phone'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="default"
              
              value={this.state.phone}
              onChangeText={(phone) => this.setState({phone: phone})}
            />
          </View>
          <View style={[styles.action, {marginTop: 30}]}>
            <TextInput
              placeholder={'Password'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="default"
              value={this.state.password}
              onChangeText={(password) => this.setState({password: password})}
              secureTextEntry={this.state.secureTextEntry}
            />
          </View>
          <View style={[styles.action, {marginTop: 30}]}>
            <TextInput
              placeholder={'Company'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="email-address"
             
              value={this.state.company}
              onChangeText={(company) => this.setState({company: company})}
            />
          </View>
          <View style={[styles.action, {marginTop: 30}]}>
            <TextInput
              placeholder={'Job Title'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="default"
             
              value={this.state.job_title}
              onChangeText={(job_title) => this.setState({job_title:job_title}) }
            />
          </View>

          <View style={[styles.action, {marginTop: 30}]}>
            <TextInput
              placeholder={'Select Sector'}
              placeholderTextColor="#ccc"
              style={[styles.textInput]}
              autoCapitalize="none"
              selectionColor={'#fff'}
              keyboardType="email-address"
              value={this.state.job_profile}
              onChangeText={(job_profile) => this.setState({job_profile:job_profile})}
            />
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              style={styles.signIn}
              onPress={()=>this.handleLoginSubmit()}>
              <LinearGradient
                colors={['#08d4c4', '#01ab9d']}
                style={styles.signIn}>
                <Text
                  style={[
                    styles.textSign,
                    {
                      color: '#FFFFFF',
                    },
                  ]}>
                  {'Create Account'}
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
                <Text style={styles.forgetText}>{"Don't have an account?"}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <OrientationLoadingOverlay
                        visible={this.props.userRegisterData.registerData.isLoading}
                        color="white"
                        indicatorSize="large"
                        messageFontSize={24}
                        message="Loading Please Wait..."
                    />
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
    userRegisterData: state,
});

const mapDispatchToProps = {
  RegisterUserAsync
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);

const {height} = Dimensions.get('screen');
const height_logo = height * 0.15;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
  },
  footer: {
    paddingHorizontal: 30,
    paddingVertical: 30,
  },
  text_header: {
    justifyContent: 'center',
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 15,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#FFFFFF',
    fontWeight: 'bold',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  colors: {
    color: '#000000',
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  forgetText: {
    color: '#ffffff',
    marginTop: 15,
    fontWeight: 'bold',
  },
});
