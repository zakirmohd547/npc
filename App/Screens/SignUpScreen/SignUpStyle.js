import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  topView: {
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  iconStyle: {
    height: 100,
    width: 100,
  },
  container: {
    justifyContent: 'center',
    marginTop:60,
  },
  textView: {
    alignItems: 'center',
  }
});

export default styles;