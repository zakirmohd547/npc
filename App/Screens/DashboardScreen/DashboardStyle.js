import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  MainContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  sliderView: {
    width: wp('100%'),
    alignContent: 'center',
  },
  offersView: {
    width: wp('98%'),
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: hp('2%'),
  },
  renderImage: {
    height: hp('20%'),
    width: wp('31%'),
    marginLeft: wp('1.5%'),
    marginBottom: wp('1.5%'),
    borderRadius: 5,
    position: 'relative', // because it's parent
    elevation: 5,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  renderText: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 16,
    position: 'absolute',
    top: hp('10'), // child
    bottom: 0, // position where you want
    left: wp('6'),
  },
  offerText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: 'white',
    position: 'absolute',
    top: hp('13'), // child
    bottom: 0, // position where you want
    left: wp('6'),
  },
});

export default styles;
