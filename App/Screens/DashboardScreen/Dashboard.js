import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  ImageBackground,
} from 'react-native';
import styles from './DashboardStyle';
import HomeHeader from '../../Components/CustomHeader/CustomeHeader';
import {FlatListSlider} from 'react-native-flatlist-slider';
const images = [
  {
    banner: require('../../Assets/Images/slide.jpg'),
    desc: 'Silent Waters in the mountains in midst of Himilayas',
  },
  {
    banner: require('../../Assets/Images/slide1.jpg'),
    desc:
      'Red fort in India New Delhi is a magnificient masterpeiece of humans',
  },
  {
    banner: require('../../Assets/Images/slide2.jpg'),
    desc:
      'Red fort in India New Delhi is a magnificient masterpeiece of humans',
  },
  {
    banner: require('../../Assets/Images/slide3.jpg'),
    desc:
      'Red fort in India New Delhi is a magnificient masterpeiece of humans',
  },
];
export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offersData: [
        {
          id: 1,
          image: require('../../Assets/Images/mens.jpg'),
          cat_name: 'Mens',
          offers: '60% Off',
        },
        {
          id: 2,
          image: require('../../Assets/Images/girls.jpg'),
          cat_name: 'Girls',
          offers: '55% Off',
        },
        {
          id: 3,
          image: require('../../Assets/Images/kids.jpg'),
          cat_name: 'Kids',
          offers: '65% Off',
        },
        {
          id: 4,
          image: require('../../Assets/Images/shoesMen.jpg'),
          cat_name: 'Shoes Mens',
          offers: '30% Off',
        },
        {
          id: 5,
          image: require('../../Assets/Images/shoesGirls.jpg'),
          cat_name: 'Shoes Girls',
          offers: '40% Off',
        },
        {
          id: 6,
          image: require('../../Assets/Images/men_girls.jpg'),
          cat_name: 'Men & Girls',
          offers: '40% Off',
        },
      ],
    };
  }

  renderItems = ({item, index}) => {
    return (
      <ImageBackground
        resizeMode="cover"
        imageStyle={{opacity: 0.8}}
        source={item.image}
        style={styles.renderImage}>
        <Text style={styles.renderText}>{item.cat_name}</Text>
        <Text style={styles.offerText}>{item.offers}</Text>
      </ImageBackground>
    );
  };

  render() {
    return (
      <SafeAreaView>
        <HomeHeader title={'Home'} value={0} />
        <View style={styles.MainContainer}>
            <Text style={{textAlign:'center',fontSize:15,marginTop:'50%'}}>Dashboard</Text>
        </View>
      </SafeAreaView>
    );
  }
}
