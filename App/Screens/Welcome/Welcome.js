/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {Avatar} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Layout from '../../Components/CustomLayout/Layout';

const Welcome = ({navigation}) => {
  return (
    <Layout style={styles.container}>
      <View style={styles.header}>
        <Avatar size={200} source={require('../../Assets/Images/logo.jpeg')} />
        <TouchableOpacity
            onPress={() => navigation.navigate('Login')}
            style={[
              styles.signIn,
              {
                borderColor: '#ccc',
                backgroundColor: '#191970',
                borderWidth: 1,
                marginTop: 20,
              },
            ]}>
            <Text
              style={[
                styles.textSign,
                {
                  color: '#ffffff',
                },
              ]}>
              {'Login'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('SignUp')}
            style={[
              styles.signIn,
              {
                borderColor: '#ccc',
                backgroundColor: '#191970',
                borderWidth: 1,
                marginTop: 20,
              },
            ]}>
            <Text
              style={[
                styles.textSign,
                {
                  color: '#ffffff',
                },
              ]}>
              {'Regisration'}
            </Text>
          </TouchableOpacity>
      </View>
    </Layout>
  );
};

export default Welcome;

const {height, width} = Dimensions.get('screen');

const height_logo = height * 0.1;
const width_logo = width * 0.7;

const height_image = height * 0.25;
const width_image = width * 0.5;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 80,
  },
  appLogo: {
    height: height_logo,
    width: width_logo,
  },
  image: {
    height: height_image,
    width: width_image,
  },
  button: {
    alignItems: 'center',
    marginTop: 35,
  },
  textSign: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
  },
  signIn: {
    width: '60%',
    height: 45,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: '#0000FF',
    borderColor: '#ccc',
  },
  imageContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
});
