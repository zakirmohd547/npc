import React from 'react';
import {View, Text, ImageBackground, Image, StyleSheet} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import * as AsyncStorages from '../../Utility/asyncStorage';
import images from '../../Utility/images';
import styles from './SplashStyle';
class Splash extends React.Component {
  performTimeConsumingTask = () => {
    return new Promise(resolve =>
      setTimeout(async () => {
        const token = await AsyncStorages.getItem(AsyncStorages.KEY_USERDATA);
        resolve(token);
      }, 3000),
    );
  };

  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();
    this.props.navigation.navigate('Welcome');
    // if (data !== null) {
    //   this.props.navigation.navigate('Dashboard');
    // } else {
    //   // Redirect user to Login page if token is not valid
    //   console.log('token null', data);
    //   this.props.navigation.navigate('Login');
    // }
  }

  render() {
    return (
      <ImageBackground
        source={require('../../Assets/Images/bg.jpeg')}
        style={styles.container}>

      </ImageBackground>
    );
  }
}

export default Splash;
